const express = require('express');
const cors = require("cors");
const hbs = require("express-hbs");
const path = require("path");

require('dotenv').config();
const MainController = require("./controllers/main.controller");


const app = express();
app.set("view engine", "hbs");
app.engine("hbs",  require("./views/helpers")(hbs).express3({partialsDir: "./views"}));
app.set("views","./views");
app.use(express.static("views"));
app.use(express.json());
app.use(cors());

new MainController(app);

hbs.registerHelper('person', function () {
    return this;
})

hbs.registerHelper('boldNumber', function () {
    let checkNumberLength = this.phone.replace(/[^0-9]/g,'');
    if (checkNumberLength.length > 10)
        return "<td style='font-weight: bold'>" + this.phone + "</td>";
        return "<td>" + this.phone + "</td>";
})

hbs.registerHelper("colorChange", function(firstname) {
    if (this.company === "Microsoft")
        return "<td style='color:green'>" + this.company + "</td>";
        return "<td>" + this.company + "</td>";
});

app.listen(process.env.PORT, () => {
    console.log(`Server is up on port ${process.env.PORT}!`);
});
